package lisp;
/*
 * Copyright (C) 2013, 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.junction.ChannelJunction;
/*
 * Created by andreasm 03.03.13 08:24
 */
public final class Channel extends ChannelJunction
{
    /**
     * Constructor for Channel
     *
     * @param capacity Capacity for unmatched Sends
     */
    public Channel(int capacity)
    {
        super(capacity);
    }
    /**
     * @see lisp.Sexpression#getType()
     */
    public String getType()
    {
        return "channel";
    }
    /**
     * @see Object#toString()
     */
    @Override
    public String toString()
    {
        return "#.(throw (quote error) \"channels cannot be read or written\")";
    }
}