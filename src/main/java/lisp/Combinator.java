/*
 * Copyright (C) 2001, 2007, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp;
/*
 * Erstellungsdatum: (24.08.01 18:14:05)
 *
 * @author Andreasm
 */
public abstract class Combinator extends Constant implements Function
{
    private final int quotemask;
    private final int parametercount;
    /**
     * Constructor for  Combinator
     *
     * @param quotemask  Set bits to mark Parameters as quoted
     * @param parametercount Count of Parameters
     */
    protected Combinator(int quotemask, int parametercount)
    {
        super();
        //
        this.quotemask = quotemask;
        this.parametercount = parametercount;
    }
    /**
     * Checks if there are quoted Parameters
     *
     * @return true, if there are quoted Parameters
     */
    public final boolean hasQuotedParameters()
    {
        return quotemask != 0;
    }
    /**
     * Returns the Parameter Count
     *
     * @return Parameter Count
     */
    public final int getParameterCount()
    {
        return parametercount;
    }
    /**
     * @see Function#isMacro()
     */
    public final boolean isMacro()
    {
        return false;
    }
    /**
     * @see Function#mustBeQuoted(int)
     */
    public final boolean mustBeQuoted(int position)
    {
        return ((quotemask >> position) & 1) == 1;
    }
    /**
     * @see Function#acceptsArgumentCount(int)
     */
    public final boolean acceptsArgumentCount(int argumentcount)
    {
        return parametercount == argumentcount;
    }
    /**
     * @see Sexpression#getType()
     */
    public final String getType()
    {
        return "combinator";
    }
}