package lisp;
/*
 * Copyright (C) 2011, 2012, 2020 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.compiler.Compiler;
import lisp.environment.Environment;
/*
 * Created by andreasm on 14.10.11 at 15:57
 */
@SuppressWarnings("WeakerAccess")
public abstract class ParameterizedBody extends Constant
{
    protected int parametercount;
    protected List parameters;
    private Sexpression body;
    protected Sexpression compiledbody;
    private Environment environment;
    /**
     * Constructor for ParameterizedBody
     *
     * @param parameters Parameters
     * @param body Body
     * @param compiledbody Compiled Body
     * @param environment Environment
     */
    public ParameterizedBody(List parameters, Sexpression body, Sexpression compiledbody, Environment environment)
    {
        super();
        //
        this.parameters = parameters;
        this.body = body;
        this.compiledbody = compiledbody;
        this.environment = environment;
        //
        parametercount = List.length(parameters);
    }
    /**
     * Sets the Parameters, Body and Environment for use in Mashaller only
     *
     * @param parameters List of Parameters
     * @param body Body
     * @param environment Environment
     * @throws CannotEvalException if compiling the Body was not successful
     */
    public void setUnmarshalled(List parameters, Sexpression body, Environment environment) throws CannotEvalException
    {
        var compiler = new Compiler(parameters, body, environment);
        compiler.compile();
        compiledbody = compiler.getResult();
        parametercount = List.length(parameters);
        //
        this.parameters = parameters;
        this.body = body;
        this.environment = environment;
    }
    /**
     * Returns the Parameters
     *
     * @return List of Parameters
     */
    public final List getParameters()
    {
        return parameters;
    }
    /**
     * Returns the Count of Parameters
     *
     * @return Count of Parameters
     */
    public final int getParameterCount()
    {
        return parametercount;
    }
    /**
     * Returns the uncompiled Body
     *
     * @return Body of Lambda, Mlambda or GuardedMethod
     */
    public final Sexpression getBody()
    {
        return body;
    }
    /**
     * Returns the Body
     *
     * @return Body of Lambda, Mlambda or GuardedMethod
     */
    public final Sexpression getCompiledBody()
    {
        return compiledbody;
    }
    /**
     * Returns the Environment
     *
     * @return Environment for evaluation of Body
     */
    public final Environment getEnvironment()
    {
        return environment;
    }
    /**
     * Binds the Parameters to the Arguments
     *
     * @param arguments Arguments for Parameters
     * @return Environment with bound Parameters
     */
    public final Environment bind(Sexpression[] arguments)
    {
        if (parametercount == 0) return environment;
        //
        return new Environment(environment, parameters, arguments);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        var builder = new StringBuilder();
        builder.append("(");
        builder.append(getType());
        builder.append(" ");
        //
        if (parameters == null)
        {
            builder.append("()");
        }
        else
        {
            builder.append(parameters);
        }
        //
        builder.append(" ");
        //
        if (body == null)
        {
            builder.append("nil");
        }
        else
        {
            builder.append(body);
        }
        //
        builder.append(")");
        //
        return builder.toString();
    }
}