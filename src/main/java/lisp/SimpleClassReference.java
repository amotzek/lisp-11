package lisp;
/*
 * Copyright (C) 2011, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.lang.ref.WeakReference;
/*
 * Created by andreasm on 28.10.11 at 21:08
 */
final class SimpleClassReference extends WeakReference<SimpleClass>
{
    SimpleClassReference(SimpleClass simpleclass)
    {
        super(simpleclass);
    }
}
