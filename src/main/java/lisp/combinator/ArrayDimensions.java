package lisp.combinator;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.List;
import lisp.Rational;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Created by andreasm on 28.04.2013
 */
@SuppressWarnings("WeakerAccess")
public final class ArrayDimensions extends TypeCheckCombinator
{
    /**
     * Constructor for ArrayDimensions
     */
    public ArrayDimensions()
    {
        super(0, 1);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        var array = getArray(arguments, 0);
        var dimensions = array.getDimensions();
        List list = null;
        //
        for (int index = dimensions.length - 1; index >= 0; index--)
        {
            var rational = new Rational(dimensions[index]);
            list = new List(rational, list);
        }
        //
        return list;
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "array-dimensions";
    }
}