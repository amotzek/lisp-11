package lisp.combinator;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.SimpleClass;
import lisp.Symbol;
import lisp.environment.Environment;
import lisp.environment.NotBoundException;
import lisp.trait.ValidationException;
import lisp.trait.ValidatorFactory;
import java.util.LinkedList;
/*
 * Erstellungsdatum: (4.1.2013)
 *
 * @author Andreasm
 */
@SuppressWarnings("WeakerAccess")
public final class DefTrait extends TypeCheckCombinator
{
    /**
     * Constructor for DefTrait
     */
    public DefTrait()
    {
        super(3, 2);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        var classname = getSymbol(arguments, 0, false);
        var superclassnames = getList(arguments, 1);
        var superclasses = new LinkedList<SimpleClass>();
        //
        try
        {
            var value = environment.at(classname);
            //
            if (value instanceof SimpleClass) return value;
            //
            if (value != null) throw new CannotEvalException(classname + " already set");
        }
        catch (NotBoundException e)
        {
            // pass
        }
        //
        try
        {
            var list = superclassnames;
            //
            while (list != null)
            {
                var superclassname = (Symbol) list.first();
                var superclass = (SimpleClass) environment.at(superclassname);
                superclasses.addLast(superclass);
                list = list.rest();
            }
        }
        catch (ClassCastException e)
        {
            throw new CannotEvalException(superclassnames + " is not a list of trait names");
        }
        catch (NotBoundException e)
        {
            throw new CannotEvalException(e.getUnboundSymbol() + " is not a trait name");
        }
        //
        try
        {
            var simpleclass = new SimpleClass(classname, false, superclasses);
            var validator = ValidatorFactory.createValidator();
            validator.validateClass(simpleclass);
            environment.add(true, classname, simpleclass);
            //
            return simpleclass;
        }
        catch (IllegalArgumentException e)
        {
            throw new CannotEvalException(superclassnames + " contains a class name but traits can only extend other traits");
        }
        catch (ValidationException e)
        {
            throw new CannotEvalException(e.getMessage());
        }
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "deftrait";
    }
}