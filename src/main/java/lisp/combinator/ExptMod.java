package lisp.combinator;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Rational;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (16.01.2018)
 *
 * @author Andreasm
 */
@SuppressWarnings("WeakerAccess")
public final class ExptMod extends TypeCheckCombinator
{
    /**
     * Constructor for ExptMod
     */
    public ExptMod()
    {
        super(0, 3);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        var value = getRational(arguments, 0);
        var exponent = getRational(arguments, 1);
        var module = getRational(arguments, 2);
        //
        if (module.isZero() || module.isNegative()) throw new CannotEvalException("module must be greater than 0");
        //
        try
        {
            return new Rational(value.bigIntegerValue().modPow(exponent.bigIntegerValue(), module.bigIntegerValue()));
        }
        catch (ArithmeticException e)
        {
            throw new CannotEvalException("arguments are not coprime");
        }
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "exptmod";
    }
}