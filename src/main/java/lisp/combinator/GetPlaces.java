package lisp.combinator;
/*
 * Copyright (C) 2013, 2020 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.List;
import lisp.Sexpression;
import lisp.environment.Environment;
import lisp.remote.server.ServerFactory;
import java.io.IOException;
/*
 * Created by andreasm 17.10.13 21:34
 */
@SuppressWarnings("WeakerAccess")
public final class GetPlaces extends TypeCheckCombinator
{
    public GetPlaces()
    {
        super(0, 0);
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments)
    {
        try
        {
            var server = ServerFactory.getServer(true);
            var places = server.getPlaces();
            List list = null;
            //
            for (var place : places)
            {
                list = new List(place, list);
            }
            //
            return list;
        }
        catch (IOException e)
        {
            return null;
        }
    }
    //
    @Override
    public String toString()
    {
        return "get-places";
    }
}