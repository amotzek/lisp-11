package lisp.combinator;
/*
 * Copyright (C) 2005, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.Rational;
import lisp.Sexpression;
import lisp.environment.Environment;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
/*
 * Erstellungsdatum: (14.05.2005 19:02)
 *
 * @author Andreasm
 */
public final class Hash extends TypeCheckCombinator
{
    /**
     * Constructor for Hash
     */
    public Hash()
    {
        super(0, 1);
    }
    /**
     * Returns the bytes for a String
     *
     * @param string String
     * @return String as Byte Array
     */
    private static byte[] getBytes(String string)
    {
        return string.getBytes(StandardCharsets.UTF_8);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        var list = getNonEmptyList(arguments, 0);
        //
        try
        {
            var messagedigest = MessageDigest.getInstance("SHA1");
            //
            while (list != null)
            {
                var sexpression = list.first();
                //
                if (sexpression != null)
                {
                    if (sexpression instanceof Chars)
                    {
                        var chars = (Chars) sexpression;
                        var string = chars.getString();
                        messagedigest.update(getBytes(string));
                    }
                    else if (sexpression instanceof Rational)
                    {
                        var rational = (Rational) sexpression;
                        var numerator = rational.getNumerator();
                        var denominator = rational.getDenominator();
                        messagedigest.update(numerator.toByteArray());
                        //
                        if (!denominator.equals(BigInteger.ONE)) messagedigest.update(denominator.toByteArray());
                    }
                    else
                    {
                        var string = sexpression.toString();
                        messagedigest.update(getBytes(string));
                    }
                }
                //
                list = list.rest();
            }
            //
            var bytes = messagedigest.digest();
            var biginteger = new BigInteger(bytes);
            //
            return new Rational(biginteger);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new CannotEvalException("no such algorithm: sha1");
        }
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "hash";
    }
}