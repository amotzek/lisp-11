package lisp.combinator;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (25.12.2021)
 *
 * @author andreasm
 */
@SuppressWarnings("WeakerAccess")
public final class IsSlotBound extends Predicate
{
    /**
     * Constructor for IsSlotBound
     */
    public IsSlotBound()
    {
        super(0, 2);
    }
    /**
     * @see TypeCheckCombinator#apply(Environment, Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        var object = getObject(arguments, 0);
        var key = getSymbol(arguments, 1, false);
        //
        return createBoolean(object.containsKey(key));
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "slot-bound?";
    }
}