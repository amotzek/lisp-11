package lisp.combinator;
/*
 * Copyright (C) 2001, 2010, 2011 - 2013, 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.List;
import lisp.Sexpression;
import lisp.compiler.Compiler;
import lisp.compiler.LambdaFactory;
import lisp.environment.Environment;
/*
 * @author Andreasm
 */
public final class Lambda extends TypeCheckCombinator
{
    /**
     * Constructor for Lambda
     */
    public Lambda()
    {
        super(3, 2);
    }
    /**
     * Checks if the Lambda can be wrapped into a TypeCheckCombinator
     *
     * @param parameters Parameters
     * @param body Body
     * @return true if the Body can be represented by a InlineLambda
     */
    private static boolean isSimple(List parameters, Sexpression body)
    {
        int parametercount = List.length(parameters);
        //
        if (parametercount > 3) return false;
        //
        if (!(body instanceof List)) return false;
        //
        var list = (List) body;
        var first = list.first();
        //
        if (!(first instanceof TypeCheckCombinator)) return false;
        //
        if (first instanceof LambdaFactory) return false; // needs the correct environment and cannot be inlined
        //
        var combinator = (TypeCheckCombinator) first;
        //
        list = list.rest();
        int position = 0;
        //
        while (list != null)
        {
            var element = list.first();
            //
            if (!isConstant(element))
            {
                if (!isIn(element, parameters)) return false;
                //
                if (combinator.mustBeQuoted(position)) return false;
            }
            //
            list = list.rest();
            position++;
        }
        //
        return true;
    }
    /**
     * Checks if Element is in List
     *
     * @param element Element
     * @param list List
     * @return true, if Element is in List
     */
    private static boolean isIn(Sexpression element, List list)
    {
        while (list != null)
        {
            if (element == list.first()) return true;
            //
            list = list.rest();
        }
        //
        return false;
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        var parameters = getList(arguments, 0);
        var body = getSexpression(arguments, 1);
        var compiler = new Compiler(parameters, body, environment);
        compiler.compile();
        var compiledbody = compiler.getResult();
        //
        if (isSimple(parameters, compiledbody)) return new InlinedLambda(parameters, (List) compiledbody);
        //
        return new lisp.Lambda(parameters, body, compiledbody, environment);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "lambda";
    }
}