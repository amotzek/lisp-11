package lisp.combinator;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (11.01.2018)
 *
 * @author Andreasm
 */
@SuppressWarnings("WeakerAccess")
public final class LogBit extends Predicate
{
    /**
     * Constructor for LogBit
     */
    public LogBit()
    {
        super(0, 2);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        var index = getRational(arguments, 0);
        var value = getRational(arguments, 1);
        //
        if (index.isNegative()) throw new CannotEvalException("index must not be negative");
        //
        return createBoolean(value.bigIntegerValue().testBit(index.intValue()));
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "logbit?";
    }
}