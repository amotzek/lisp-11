package lisp.combinator;
/*
 * Copyright (C) 2001, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.List;
import lisp.Sexpression;
import lisp.compiler.Compiler;
import lisp.environment.Environment;
//
@SuppressWarnings("WeakerAccess")
public final class Mlambda extends TypeCheckCombinator
{
    /**
     * Constructor for Mlambda
     */
    public Mlambda()
    {
        super(3, 2);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        var parameter = getSymbol(arguments, 0, false);
        var body = getSexpression(arguments, 1);
        var parameters = new List(parameter, null);
        var compiler = new Compiler(parameters, body, environment);
        compiler.compile();
        var compiledbody = compiler.getResult();
        //
        return new lisp.Mlambda(parameters, body, compiledbody, environment);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "mlambda";
    }
}