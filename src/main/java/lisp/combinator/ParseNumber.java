package lisp.combinator;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Rational;
import lisp.Sexpression;
import lisp.environment.Environment;
import lisp.parser.Parser;
/*
 * Erstellungsdatum: (16.12.2021)
 *
 * @author Andreasm
 */
@SuppressWarnings("WeakerAccess")
public final class ParseNumber extends TypeCheckCombinator
{
    /**
     * @see Object#Object()
     */
    public ParseNumber()
    {
        super(0, 1);
    }
    /**
     * @see TypeCheckCombinator#apply(Environment, Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        var chars = getChars(arguments, 0);
        //
        try
        {
            return new Rational(chars.getString());
        }
        catch (NumberFormatException e)
        {
            throw new CannotEvalException("malformed number in " + chars);
        }
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "parse-number";
    }
}