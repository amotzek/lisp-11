/*
 * Copyright (C) 2001, 2010 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp.combinator;
//
import lisp.Sexpression;
import lisp.Symbol;
/*
 * Erstellungsdatum: (25.08.2001)
 *
 * @author Andreasm
 */
public abstract class Predicate extends TypeCheckCombinator
{
    private static Symbol T = Symbol.createSymbol("t");
    /**
     * Constructor for Predicate
     *
     * @param quotemask  Bitmask for quoted parameters
     * @param parameters Count of parameters
     */
    protected Predicate(int quotemask, int parameters)
    {
        super(quotemask, parameters);
    }
    /**
     * Returns t or nil
     *
     * @param b True or False
     * @return T or Nil
     */
    protected static Sexpression createBoolean(boolean b)
    {
        if (b) return T;
        //
        return null;
    }
}