package lisp.combinator;
/*
 * Copyright (C) 2002, 2010 - 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.*;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.continuation.TraceBuilder;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * Erstellungsdatum: (21.02.2002 01:06:17)
 *
 * @author Andreasm
 */
@SuppressWarnings({"WeakerAccess", "SameParameterValue", "ConstantConditions"})
public abstract class TypeCheckCombinator extends Combinator
{
    /**
     * Constructor for TypeCheckCombinator
     *
     * @param quotemask  Bitmask for quoted parameters
     * @param parameters Count of parameters
     */
    protected TypeCheckCombinator(int quotemask, int parameters)
    {
        super(quotemask, parameters);
    }
    /**
     * Method apply
     *
     *
     * @param environment Environment for execution
     * @param arguments   Array of Arguments
     * @return Value
     * @throws CannotEvalException if Combinator cannot be applied to Arguments
     */
    public abstract Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException;
    /**
     * @see lisp.Function#apply(lisp.concurrent.RunnableQueue, lisp.environment.Environment, lisp.Sexpression[], lisp.continuation.SuccessContinuation, lisp.continuation.FailureContinuation)
     */
    public final void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        try
        {
            var value = apply(environment, arguments);
            succeed.succeed(value);
        }
        catch (CannotEvalException e)
        {
            var builder = new TraceBuilder(e, succeed);
            builder.build();
            builder.invoke(fail);
        }
    }
    /**
     * Method getArray
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected Array getArray(Sexpression[] arguments, int index) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (sexpr != null) return (Array) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("array", sexpr));
    }
    /**
     * Method getClass
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected SimpleClass getClass(Sexpression[] arguments, int index) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (sexpr != null) return (SimpleClass) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("class", sexpr));
    }
    /**
     * Method getChars
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected Chars getChars(Sexpression[] arguments, int index) throws CannotEvalException
    {
        return getChars(arguments, index, false);
    }
    /**
     * Method getChars
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @param optional  if null is allowed as result
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected Chars getChars(Sexpression[] arguments, int index, boolean optional) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (optional || sexpr != null) return (Chars) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("string", sexpr));
    }
    /**
     * Method getCombinator
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected TypeCheckCombinator getTypeCheckCombinator(Sexpression[] arguments, int index) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (sexpr != null) return (TypeCheckCombinator) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("combinator", sexpr));
    }
    /**
     * Method getChannel
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected Channel getChannel(Sexpression[] arguments, int index) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (sexpr != null) return (Channel) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("channel", sexpr));
    }
    /**
     * Method getComparable
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected Comparable getComparable(Sexpression[] arguments, int index) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (sexpr != null) return (Comparable) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("comparable", sexpr));
    }
    /**
     * Method getAssociativeContainer
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected AssociativeContainer getAssociativeContainer(Sexpression[] arguments, int index) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (sexpr != null) return (AssociativeContainer) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("array, hash-table or instance", sexpr));
    }
    /**
     * Method getFunction
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected Function getFunction(Sexpression[] arguments, int index) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (sexpr != null) return (Function) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("function", sexpr));
    }
    /**
     * Method getGenericFunction
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected GenericFunction getGenericFunction(Sexpression[] arguments, int index) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (sexpr != null) return (GenericFunction) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("generic-function", sexpr));
    }
    /**
     * Method getHashTable
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected HashTable getHashTable(Sexpression[] arguments, int index) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (sexpr != null) return (HashTable) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("hash-table", sexpr));
    }
    /**
     * Method getList
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected List getList(Sexpression[] arguments, int index) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            return (List) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("list", sexpr));
    }
    /**
     * Method getMethod
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected GuardedMethod getMethod(Sexpression[] arguments, int index) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (sexpr != null) return (GuardedMethod) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("method", sexpr));
    }
    /**
     * Method getNonEmptyList
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected List getNonEmptyList(Sexpression[] arguments, int index) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (sexpr != null) return (List) sexpr;
            //
            throw new CannotEvalException(createErrorMessage("non-empty list", sexpr));
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("list", sexpr));
    }
    /**
     * Method getObject
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected RubyStyleObject getObject(Sexpression[] arguments, int index) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (sexpr != null) return (RubyStyleObject) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("instance", sexpr));
    }
    /**
     * Method getParameterizedBody
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected ParameterizedBody getParameterizedBody(Sexpression[] arguments, int index) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (sexpr != null) return (ParameterizedBody) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("lambda, mlambda or method", sexpr));
    }
    /**
     * Method getRational
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected Rational getRational(Sexpression[] arguments, int index) throws CannotEvalException
    {
        return getRational(arguments, index, false);
    }
    /**
     * Method getRational
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @param optional  if null is allowed as result
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected Rational getRational(Sexpression[] arguments, int index, boolean optional) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (optional || sexpr != null) return (Rational) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("number", sexpr));
    }
    /**
     * Method getSexpression
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @return Argument at given Index
     */
    protected Sexpression getSexpression(Sexpression[] arguments, int index)
    {
        return arguments[index];
    }
    /**
     * Method getSymbol
     *
     * @param arguments Array of Arguments
     * @param index     Index
     * @param optional  if nil is accepted as valid Argument
     * @return Argument at given Index
     * @throws CannotEvalException if Argument has wrong type
     */
    protected Symbol getSymbol(Sexpression[] arguments, int index, boolean optional) throws CannotEvalException
    {
        var sexpr = arguments[index];
        //
        try
        {
            if (optional || sexpr != null) return (Symbol) sexpr;
        }
        catch (ClassCastException e)
        {
            // pass
        }
        //
        throw new CannotEvalException(createErrorMessage("atom", sexpr));
    }
    /**
     * Creates an Error Message
     *
     * @param type     Type expected
     * @param argument Argument
     * @return String Error Message
     */
    protected final String createErrorMessage(String type, Sexpression argument)
    {
        var builder = new StringBuilder();
        builder.append(argument);
        builder.append(" is not a ");
		builder.append(type);
        builder.append(" in ");
        builder.append(this);
		//
		return builder.toString();
	}
}