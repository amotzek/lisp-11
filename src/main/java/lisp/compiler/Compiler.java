package lisp.compiler;
/*
 * Copyright (C) 2012 - 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import static lisp.List.reverse;
import lisp.CannotEvalException;
import lisp.Closure;
import lisp.Function;
import lisp.List;
import lisp.Mlambda;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.combinator.*;
import lisp.compiler.aggregator.Aggregator;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueueFactory;
/*
 * Created by andreasm 24.07.12 19:23
 */
public final class Compiler
{
    private final List parameters;
    private final Environment environment;
    private final Sexpression source;
    private Sexpression result;
    /**
     * Constructor for Compiler
     *
     * @param parameters  List of Parameters
     * @param source      S-Expression to compile
     * @param environment Environment
     */
    public Compiler(List parameters, Sexpression source, Environment environment)
    {
        super();
        //
        this.parameters = parameters;
        this.environment = environment;
        this.source = source;
    }
    //
    private static Sexpression compile(Sexpression sexpression, boolean isroot, Scope scope) throws CannotEvalException
    {
        if (sexpression instanceof List) return compileList((List) sexpression, isroot, scope);
        //
        return sexpression;
    }
    //
    private static Sexpression compileList(List list, boolean isroot, Scope scope) throws CannotEvalException
    {
        var first = list.first();
        var rest = list.rest();
        //
        if (first instanceof Symbol) first = scope.getSymbolOrValue((Symbol) first);
        //
        if (first instanceof If) return compile3(first, rest, scope);
        //
        if (first instanceof Or2) return compile2(first, rest, scope);
        //
        if (first instanceof Lambda) return compileLambda(rest, scope);
        //
        if (first instanceof Letrec) return compileLetrec(first, rest, scope);
        //
        if (first instanceof Sequential) return compileSequential(first, rest, scope);
        //
        if (first instanceof CatchAndApply) return compile3(first, rest, scope);
        //
        if (first instanceof Mlambda) return expandAndCompileMacro((Mlambda) first, rest, scope);
        //
        if (first instanceof Function) return compileFunction((Function) first, rest, isroot, scope);
        //
        return list;
    }
    //
    private static Sexpression compile3(Sexpression function, List arguments, Scope scope) throws CannotEvalException
    {
        var argument1 = arguments.first();
        arguments = arguments.rest();
        var argument2 = arguments.first();
        arguments = arguments.rest();
        var argument3 = arguments.first();
        arguments = arguments.rest();
        //
        if (arguments != null) throw new CannotEvalException("wrong number of arguments for " + function);
        //
        var translatedargument1 = compile(argument1, true, scope);
        var translatedargument2 = compile(argument2, true, scope);
        var translatedargument3 = compile(argument3, true, scope);
        var translatedlist = new List(translatedargument3, null);
        translatedlist = new List(translatedargument2, translatedlist);
        translatedlist = new List(translatedargument1, translatedlist);
        translatedlist = new List(function, translatedlist);
        //
        return translatedlist;
    }
    //
    private static Sexpression compile2(Sexpression function, List arguments, Scope scope) throws CannotEvalException
    {
        var argument1 = arguments.first();
        arguments = arguments.rest();
        var argument2 = arguments.first();
        arguments = arguments.rest();
        //
        if (arguments != null) throw new CannotEvalException("wrong number of arguments for " + function);
        //
        var translatedargument1 = compile(argument1, true, scope);
        var translatedargument2 = compile(argument2, true, scope);
        //
        if (translatedargument2 == null) return translatedargument1;
        //
        var translatedlist = new List(translatedargument2, null);
        translatedlist = new List(translatedargument1, translatedlist);
        translatedlist = new List(function, translatedlist);
        //
        return translatedlist;
    }
    //
    private static Sexpression compileSequential(Sexpression function, List arguments, Scope scope) throws CannotEvalException
    {
        List translatedarguments = null;
        arguments = (List) arguments.first();
        //
        while (arguments != null)
        {
            var argument = arguments.first();
            arguments = arguments.rest();
            var translatedargument = compile(argument, true, scope);
            translatedarguments = new List(translatedargument, translatedarguments);
        }
        //
        var translatedlist = new List(reverse(translatedarguments), null);
        translatedlist = new List(function, translatedlist);
        //
        return translatedlist;
    }
    //
    private static Sexpression compileLambda(List arguments, Scope parent) throws CannotEvalException
    {
        var parameters = (List) arguments.first();
        arguments = arguments.rest();
        var body = arguments.first();
        var child = parent.pushParameters(parameters);
        var translatedbody = compile(body, true, child);
        var factory = new LambdaFactory(parameters, body, translatedbody);
        //
        return new List(factory, null);
    }
    //
    private static Sexpression compileLetrec(Sexpression function, List arguments, Scope parent) throws CannotEvalException
    {
        var definitions = (List) arguments.first();
        arguments = arguments.rest();
        Sexpression body = arguments.first();
        var parameters = getParameters(definitions);
        var child = parent.pushParameters(parameters);
        var translatedbody = compile(body, true, child);
        var translateddefinitions = compileDefinitions(definitions, child);
        var translatedlist = new List(translatedbody, null);
        translatedlist = new List(translateddefinitions, translatedlist);
        translatedlist = new List(function, translatedlist);
        //
        return translatedlist;
    }
    //
    private static List getParameters(List definitions)
    {
        List parameters = null;
        //
        while (definitions != null)
        {
            var definition = (List) definitions.first();
            var parameter = definition.first();
            parameters = new List(parameter, parameters);
            definitions = definitions.rest();
        }
        //
        return parameters;
    }
    //
    private static List compileDefinitions(List definitions, Scope scope) throws CannotEvalException
    {
        List translateddefinitions = null;
        //
        while (definitions != null)
        {
            var definition = (List) definitions.first();
            var key = definition.first();
            definition = definition.rest();
            var value = definition.first();
            var translatedvalue = compile(value, true, scope);
            var translateddefinition = new List(translatedvalue, null);
            translateddefinition = new List(key, translateddefinition);
            translateddefinitions = new List(translateddefinition, translateddefinitions);
            definitions = definitions.rest();
        }
        //
        return reverse(translateddefinitions);
    }
    //
    private static Sexpression expandAndCompileMacro(Mlambda mlambda, List arguments, Scope scope) throws CannotEvalException
    {
        /*
         * Expand the macro
         */
        var runnablequeue = RunnableQueueFactory.createPassiveRunnableQueue();
        var body = mlambda.getCompiledBody();
        var parameters = mlambda.getParameters();
        var parent = mlambda.getEnvironment();
        var child = new Environment(parent, parameters, new Sexpression[] { arguments });
        var closure = new Closure(runnablequeue, child, body);
        var intermediate = closure.eval();
        /*
         * Compile the expansion
         */
        return compile(intermediate, true, scope);
    }
    //
    private static Sexpression compileFunction(Function function, List arguments, boolean isroot, Scope scope) throws CannotEvalException
    {
        /*
         * Compile the arguments
         */
        var isaggregatable = function instanceof TypeCheckCombinator;
        var translatedlist = new List(function, null);
        int position = 0;
        //
        while (arguments != null)
        {
            var argument = arguments.first();
            var translatedargument = function.mustBeQuoted(position) ? argument : compile(argument, !isaggregatable, scope);
            translatedlist = new List(translatedargument, translatedlist);
            arguments = arguments.rest();
            position++;
        }
        //
        translatedlist = reverse(translatedlist);
        //
        if (isroot && isaggregatable)
        {
            /*
             * Aggregate nested function calls
             */
            var aggregator = new Aggregator(translatedlist);
            aggregator.aggregate();
            //
            return aggregator.getResult();
        }
        //
        return translatedlist;
    }
    /**
     * Compiles the S-Expression
     *
     * @throws CannotEvalException if the Expression is not wellformed
     */
    public void compile() throws CannotEvalException
    {
        var scope = new Scope(parameters, environment);
        //
        try
        {
            result = compile(source, true, scope);
            //
            return;
        }
        catch (CannotEvalException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            // no compilation
        }
        //
        result = source;
    }
    /**
     * Returns the Result of the Compilation
     *
     * @return Compiled S-Expression
     */
    public Sexpression getResult()
    {
        return result;
    }
}