package lisp.compiler;
//
import lisp.combinator.TypeCheckCombinator;
/*
 * Copyright (C) 2017 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
public abstract class GeneratedTypeCheckCombinator extends TypeCheckCombinator
{
    /**
     * Constructor for TypeCheckCombinator
     *
     * @param quotemask  Bitmask for quoted parameters
     * @param parameters Count of parameters
     */
    protected GeneratedTypeCheckCombinator(int quotemask, int parameters)
    {
        super(quotemask, parameters);
    }
}