/*
 * Copyright  2000-2004 The Apache Software Foundation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License. 
 *
 */
package lisp.compiler.bcel.generic;

/**
 * Super class for instructions dealing with array access such as IALOAD.
 *
 * @version $Id: ArrayInstruction.java 386056 2006-03-15 11:31:56Z tcurdt $
 * @author  <A HREF="mailto:m.dahm@gmx.de">M. Dahm</A>
 */
public abstract class ArrayInstruction extends Instruction implements ExceptionThrower,
        TypedInstruction {

    /**
     * Empty constructor needed for the Class.newInstance() statement in
     * Instruction.readInstruction(). Not to be used otherwise.
     */
    ArrayInstruction() {
    }


    /**
     * @param opcode of instruction
     */
    protected ArrayInstruction(short opcode) {
        super(opcode, (short) 1);
    }


    public Class[] getExceptions() {
        return lisp.compiler.bcel.ExceptionConstants.EXCS_ARRAY_EXCEPTION;
    }


    /** @return type associated with the instruction
     */
    public Type getType( ConstantPoolGen cp ) {
        switch (opcode) {
            case lisp.compiler.bcel.Constants.IALOAD:
            case lisp.compiler.bcel.Constants.IASTORE:
                return Type.INT;
            case lisp.compiler.bcel.Constants.CALOAD:
            case lisp.compiler.bcel.Constants.CASTORE:
                return Type.CHAR;
            case lisp.compiler.bcel.Constants.BALOAD:
            case lisp.compiler.bcel.Constants.BASTORE:
                return Type.BYTE;
            case lisp.compiler.bcel.Constants.SALOAD:
            case lisp.compiler.bcel.Constants.SASTORE:
                return Type.SHORT;
            case lisp.compiler.bcel.Constants.LALOAD:
            case lisp.compiler.bcel.Constants.LASTORE:
                return Type.LONG;
            case lisp.compiler.bcel.Constants.DALOAD:
            case lisp.compiler.bcel.Constants.DASTORE:
                return Type.DOUBLE;
            case lisp.compiler.bcel.Constants.FALOAD:
            case lisp.compiler.bcel.Constants.FASTORE:
                return Type.FLOAT;
            case lisp.compiler.bcel.Constants.AALOAD:
            case lisp.compiler.bcel.Constants.AASTORE:
                return Type.OBJECT;
            default:
                throw new ClassGenException("Oops: unknown case in switch" + opcode);
        }
    }
}
