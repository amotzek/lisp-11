package lisp.equality.strategy;
/*
 * Copyright (C) 2013, 2016, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.equality.EqualityTest;
import java.util.IdentityHashMap;
/*
 * Created by andreasm 30.04.13 19:58
 */
abstract class BaseStrategy<T extends Sexpression> implements EqualityStrategy<T>
{
    protected final EqualityTest equality;
    private final IdentityHashMap<T, T> same;
    //
    BaseStrategy(EqualityTest equality)
    {
        super();
        //
        this.equality = equality;
        //
        same = new IdentityHashMap<>();
    }
    //
    public final boolean areEqual(T sexpression1, T sexpression2)
    {
        if (sexpression1 == sexpression2) return true;
        /*
         * Mutable S-Expressions are only equal, if they are same
         */
        if (isMutable(sexpression1)) return false;
        //
        if (isMutable(sexpression2)) return false;
        /*
         * Check if we were here before in a cyclic structure
         */
        if (sexpression2 == same.get(sexpression1)) return true;
        //
        if (same.put(sexpression1, sexpression2) != null) return false;
        /*
         * Immutable S-Expressions that are not same
         * are equal if their meta data (e.g. size, class) are equal
         * and their contents are equal
         */
        return haveEqualMeta(sexpression1, sexpression2) && haveEqualContents(sexpression1, sexpression2);
    }
    //
    protected abstract boolean isMutable(T sexpression);
    //
    protected abstract boolean haveEqualMeta(T sexpression1, T sexpression2);
    //
    protected abstract boolean haveEqualContents(T sexpression1, T sexpression2);
}