package lisp.equality.strategy;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
/*
 * Created by andreas-motzek@t-online.de 02.05.13 17:18
 */
public interface EqualityStrategy<T extends Sexpression>
{
    boolean areEqual(T sexpression1, T sexpression2);
}