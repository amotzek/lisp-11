package lisp.equality.strategy;
/*
 * Copyright (C) 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.HashTable;
import lisp.Sexpression;
import lisp.equality.EqualityTest;
/*
 * Created by andreasm 30.04.13 20:33
 */
final class HashTableStrategy extends AssociativeContainerStrategy<Sexpression, HashTable>
{
    HashTableStrategy(EqualityTest equality)
    {
        super(equality);
    }
    //
    @Override
    protected boolean haveEqualMeta(HashTable hashtable1, HashTable hashtable2)
    {
        return hashtable1.size() == hashtable2.size();
    }
}