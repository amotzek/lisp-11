package lisp.formatter;
/*
 * Copyright (C) 2012, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.AssociativeContainer;
import lisp.List;
import lisp.Sexpression;
import lisp.formatter.strategy.StrategyFactory;
import java.util.HashSet;
import java.util.IdentityHashMap;
/*
 * Created by andreasm on 21.12.12 at 12:23
 */
public final class Formatter
{
    private static final Long NO_ID = 0L;
    //
    private final StringBuilder builder;
    private final IdentityHashMap<Formattable, Long> idsbyformattable;
    private final HashSet<Long> ids;
    private long nextid;
    //
    public Formatter()
    {
        super();
        //
        builder = new StringBuilder();
        idsbyformattable = new IdentityHashMap<>();
        ids = new HashSet<>();
        nextid = 1L;
    }
    //
    public void format(Sexpression sexpression)
    {
        addReferences(sexpression);
        append(sexpression);
    }
    //
    public String toString()
    {
        return builder.toString();
    }
    //
    public void append(Sexpression sexpression)
    {
        if (needsFormatting(sexpression))
        {
            var strategy = StrategyFactory.findStrategyFor(sexpression);
            strategy.format(sexpression, this);
        }
    }
    //
    public void append(String string)
    {
        builder.append(string);
    }
    //
    @SuppressWarnings("unchecked")
    private void addReferences(Sexpression sexpression)
    {
        if (addReferenceIfNeeded(sexpression))
        {
            if (sexpression instanceof List)
            {
                var list = (List) sexpression;
                var iterator = list.elementIterator();
                //
                while (iterator.hasNext())
                {
                    var element = iterator.next();
                    addReferences(element);
                }
            }
            else if (sexpression instanceof AssociativeContainer)
            {
                var container = (AssociativeContainer<Sexpression>) sexpression;
                var iterator = container.keyIterator();
                //
                while (iterator.hasNext())
                {
                    var key = iterator.next();
                    var value = container.get(key);
                    //
                    if (container.hasComplexKeys()) addReferences(key);
                    //
                    addReferences(value);
                }
            }
        }
    }
    //
    private boolean addReferenceIfNeeded(Sexpression sexpression)
    {
        if (sexpression instanceof Formattable)
        {
            var formattable = (Formattable) sexpression;
            var id = idsbyformattable.get(formattable);
            //
            if (id == null)
            {
                idsbyformattable.put(formattable, NO_ID);
                //
                return true;
            }
            //
            if (id == NO_ID) idsbyformattable.put(formattable, nextid++);
        }
        //
        return false;
    }
    //
    private boolean needsFormatting(Sexpression sexpression)
    {
        if (sexpression instanceof Formattable)
        {
            var id = idsbyformattable.get(sexpression);
            //
            if (id == null) throw new IllegalStateException();
            //
            if (id == NO_ID) return true;
            //
            if (ids.contains(id))
            {
                builder.append("#");
                builder.append(id);
                builder.append("#");
                //
                return false;
            }
            //
            ids.add(id);
            builder.append("#");
            builder.append(id);
            builder.append("=");
        }
        //
        return true;
    }
}