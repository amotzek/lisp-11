package lisp.formatter.strategy;
/*
 * Copyright (C) 2012, 2013, 2016, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.AssociativeContainer;
import lisp.Sexpression;
import lisp.formatter.Formatter;
/*
 * Created by andreasm on 21.12.12 at 12:49
 */
abstract class AssociativeContainerStrategy implements FormattingStrategy
{
    public abstract String getPreamble(Sexpression map);
    //
    public abstract String getSeparator();
    //
    public abstract String getBeforeKey(Sexpression key);
    //
    public abstract String getAfterKey(Sexpression key);
    //
    public abstract String getBeforeValue(Sexpression value);
    //
    public abstract String getAfterValue(Sexpression value);
    //
    public abstract String getAppendix(Sexpression map);
    //
    @Override
    @SuppressWarnings("unchecked")
    public final void format(Sexpression sexpression, Formatter formatter)
    {
        var container = (AssociativeContainer<Sexpression>) sexpression;
        var iterator = container.keyIterator();
        var first = true;
        formatter.append(getPreamble(sexpression));
        //
        while (iterator.hasNext())
        {
            var key = iterator.next();
            var value = container.get(key);
            //
            if (first)
            {
                first = false;
            }
            else
            {
                formatter.append(getSeparator());
            }
            //
            formatter.append(getBeforeKey(key));
            //
            if (key == null)
            {
                formatter.append("nil");
            }
            else if (container.hasComplexKeys())
            {
                formatter.append(key);
            }
            else
            {
                formatter.append(key.toString());
            }
            //
            formatter.append(getAfterKey(key));
            formatter.append(getBeforeValue(value));
            formatter.append(value);
            formatter.append(getAfterValue(value));
        }
        //
        formatter.append(getAppendix(sexpression));
    }
    //
    protected static boolean isConstant(Sexpression expression)
    {
        if (expression == null) return true;
        //
        return expression.isConstant();
    }
}