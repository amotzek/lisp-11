package lisp.formatter.strategy;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.List;
import lisp.Sexpression;
import lisp.formatter.Formatter;
/*
 * Created by andreasm on 21.12.12 at 17:12
 */
final class ListStrategy implements FormattingStrategy
{
    public void format(Sexpression sexpression, Formatter formatter)
    {
        var formattable = (List) sexpression;
        var iterator = formattable.elementIterator();
        var first = true;
        formatter.append("(");
        //
        while (iterator.hasNext())
        {
            if (first)
            {
                first = false;
            }
            else
            {
                formatter.append(" ");
            }
            //
            var element = iterator.next();
            formatter.append(element);
        }
        //
        formatter.append(")");
    }
}