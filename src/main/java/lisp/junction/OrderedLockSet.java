package lisp.junction;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
import java.util.TreeSet;
/*
 * Created by andreasm 07.11.13 20:16
 */
final class OrderedLockSet
{
    private final TreeSet<OrderedLock> locks;
    //
    OrderedLockSet()
    {
        super();
        //
        locks = new TreeSet<>();
    }
    //
    void addLock(OrderedLock lock)
    {
        locks.add(lock);
    }
    //
    void addLocksOf(Flow flow)
    {
        locks.addAll(flow.getLocks());
    }
    //
    void addLocksOf(LinkedList<Flow> flows)
    {
        for (var flow : flows)
        {
            addLocksOf(flow);
        }
    }
    //
    void lockAll()
    {
        for (var lock : locks)
        {
            lock.lock();
        }
    }
    //
    void unlockAll()
    {
        for (var lock : locks)
        {
            lock.unlock();
        }
    }
}