package lisp.junction;
/*
 * Copyright (C) 2012, 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * Created by  andreasm 18.11.12 11:46
 */
final class TimeoutList implements Runnable
{
    private static final int LOOP_LIMIT = 32;
    private static final int ACTION_LIMIT = 256;
    //
    private static final Logger logger = Logger.getLogger("TimoutList");
    //
    private final Thread thread;
    private final PriorityQueue<TimedFlow> flows;
    private int loops;
    //
    TimeoutList()
    {
        super();
        //
        flows = new PriorityQueue<>(64);
        thread = new Thread(this);
        thread.setName("channel-timeouts");
        thread.setDaemon(true);
    }
    //
    void start()
    {
        thread.start();
    }
    //
    synchronized void add(TimedFlow flow)
    {
        flows.add(flow);
        var nextflow = flows.peek();
        //
        if (flow == nextflow) thread.interrupt();
    }
    //
    @Override
    public void run()
    {
        while (true)
        {
            var now = System.currentTimeMillis();
            processTimeouts(now);
            cleanupIfNeeded();
        }
    }
    //
    private synchronized void processTimeouts(long now)
    {
        try
        {
            var flow = flows.peek();
            //
            if (flow == null)
            {
                wait();
                //
                return;
            }
            //
            do
            {
                var validuntil = flow.getValidUntil();
                var duration = validuntil - now;
                //
                if (duration > 0L)
                {
                    wait(duration);
                    //
                    return;
                }
                //
                var locks = new OrderedLockSet();
                locks.addLocksOf(flow);
                locks.lockAll();
                //
                if (!flow.isDone()) flow.abort();
                //
                locks.unlockAll();
                flows.poll();
                flow = flows.peek();
            }
            while (flow != null);
        }
        catch (InterruptedException ignored)
        {
        }
        catch (Exception e)
        {
            logger.log(Level.INFO, "unexpected exception", e);
        }
    }
    //
    private synchronized void cleanupIfNeeded()
    {
        if (++loops < LOOP_LIMIT) return;
        //
        loops = 0;
        //
        if (flows.size() < ACTION_LIMIT) return;
        //
        var nextflows = new LinkedList<TimedFlow>();
        //
        for (var flow : flows)
        {
            if (flow.wasContinued()) continue;
            //
            nextflows.add(flow);
        }
        //
        flows.clear();
        flows.addAll(nextflows);
    }
}