package lisp.remote.protocol;
/*
 * Copyright (C) 2013, 2016, 2020 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Combinator;
import lisp.Place;
import lisp.Sexpression;
import lisp.environment.Environment;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Collection;
/**
 * A Call represents the remote evaluation of
 * a S-Expression in an Environment issued by
 * one Place and executed at another.
 *
 * Created by andreasm 16.10.13 07:57
 */
public final class IncomingCall extends IncomingMessage
{
    private final Long id;
    private final Place caller;
    private final Environment environment;
    private final Sexpression expression;
    private final Marshaller marshaller;
    //
    private IncomingCall(Long id, Place caller, Environment environment, Sexpression expression, Marshaller marshaller)
    {
        super();
        //
        this.id = id;
        this.caller = caller;
        this.environment = environment;
        this.expression = expression;
        this.marshaller = marshaller;
    }
    /**
     * Parses a Call from the input stream, used in the called Place
     *
     * @param combinators Combinators
     * @param in Input stream
     * @return Call
     * @throws IOException if the data from the input stream is invalid
     */
    static IncomingCall unmarshal(Collection<Combinator> combinators, DataInputStream in) throws IOException
    {
        var id = readMessageHeader(TYPE_CALL, in);
        var caller = Marshaller.readPlace(in);
        var marshaller = new Marshaller(combinators);
        //
        try
        {
            var environment = marshaller.readEnvironment(in);
            var expression = marshaller.readExpression(in);
            //
            return new IncomingCall(id, caller, environment, expression, marshaller);
        }
        catch (IOException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new IOException("cannot unmarshal", e);
        }
    }
    /**
     * @see Message#getType()
     */
    @Override
    public int getType()
    {
        return TYPE_CALL;
    }
    /**
     * @see Message#getId()
     */
    @Override
    public Long getId()
    {
        return id;
    }
    /**
     * Returns the calling Place
     *
     * @return Calling Place
     */
    public Place getCaller()
    {
        return caller;
    }
    /**
     * Returns the Environment that is used to evaluate the Expression
     *
     * @return Environment
     */
    public Environment getEnvironment()
    {
        return environment;
    }
    /**
     * Returns the S-Expression that should be or gets evaluated
     *
     * @return S-Expression
     */
    public Sexpression getExpression()
    {
        return expression;
    }
    /**
     * Returns the Marshaller that is used to serialize or parse the Call
     *
     * @return Marshaller
     */
    Marshaller getMarshaller()
    {
        return marshaller;
    }
    /**
     * Returns an estimation on the count of objects in this call,
     * only applicable after sending or receiving
     *
     * @return Count of objects
     */
    public int size()
    {
        return marshaller.getCurrentId();
    }
}