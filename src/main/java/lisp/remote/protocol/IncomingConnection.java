package lisp.remote.protocol;
/*
 * Copyright (C) 2020 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Combinator;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Collection;
/*
 * Instances represent one-way Connections between
 * another Place and the local Place
 *
 * Created by andreasm 03.07.2020
 */
public final class IncomingConnection
{
    private static final int MESSAGE_SIZE = 524288;
    private static final int SOCKET_TIMEOUT = 30000;
    //
    private final Socket clientsocket;
    private final DataInputStream in;
    private final Object receivelock;
    /**
     * Creates a Connection from an existing Socket
     *
     * @param clientsocket Socket
     * @throws IOException if the Socket options cannot be set
     */
    IncomingConnection(Socket clientsocket) throws IOException
    {
        clientsocket.setSoTimeout(SOCKET_TIMEOUT);
        //
        this.clientsocket = clientsocket;
        //
        in = new DataInputStream(clientsocket.getInputStream());
        receivelock = new Object();
    }
    /**
     * Receives and parses a Message from this Connection
     *
     * @param protocol Protocol
     * @param combinators Combinators
     * @return Message
     * @throws IOException if a Message cannot be received or parsed
     */
    public Message receive(Protocol protocol, Collection<Combinator> combinators) throws IOException
    {
        byte[] body;
        //
        synchronized (receivelock)
        {
            int length = in.readInt();
            //
            if (length <= 0) throw new IOException("corrupted message size: " + length);
            //
            if (length > MESSAGE_SIZE) throw new IOException("excessive message size: " + length);
            //
            body = new byte[length];
            in.readFully(body);
        }
        //
        var factory = new MessageFactory(protocol, combinators, body);
        //
        return factory.getMessage();
    }
    /**
     * Closes this Connection and the Socket
     */
    public void close()
    {
        try
        {
            clientsocket.close();
        }
        catch (IOException ignored)
        {
        }
    }
}
