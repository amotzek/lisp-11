package lisp.remote.protocol;
/*
 * Copyright (C) 2013, 2014, 2016, 2020 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;
import lisp.Chars;
import lisp.Place;
import lisp.Symbol;
/*
 * Instances represent one-way Connections between
 * the local Place and another Place
 *
 * Created by andreasm 16.10.13 08:13
 */
final class OutgoingConnection
{
    private static final int MESSAGE_SIZE = 524288;
    private static final int SOCKET_TIMEOUT = 30000;
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Chars EXCESSIVE_MESSAGE_SIZE = new Chars("excessive message size");
    //
    private static final ConcurrentHashMap<Place, OutgoingConnection> connectionsbyplace = new ConcurrentHashMap<>(32);
    //
    private final Place place;
    private final Socket clientsocket;
    private final DataOutputStream out;
    private final Object sendlock;
    private volatile long lastuse;
    //
    private OutgoingConnection(Place place, Socket clientsocket) throws IOException
    {
        super();
        //
        this.place = place;
        this.clientsocket = clientsocket;
        //
        out = new DataOutputStream(clientsocket.getOutputStream());
        sendlock = new Object();
        lastuse = System.currentTimeMillis();
    }
    /**
     * Recycles a Connection to the given Place or creates a new one
     * and registers it for recycling
     *
     * @param place Place to connect to
     * @return Connection
     * @throws IOException if no TCP connection can be established
     */
    static OutgoingConnection getInstance(Place place) throws IOException
    {
        var connection = connectionsbyplace.get(place);
        //
        if (connection != null)
        {
            if (connection.canUse()) return connection;
            //
            connection.close();
        }
        //
        var host = place.getHost();
        int port = place.getPort();
        var clientsocket = new Socket(host, port);
        clientsocket.setTcpNoDelay(true);
        clientsocket.setSoTimeout(SOCKET_TIMEOUT);
        connection = new OutgoingConnection(place, clientsocket);
        connectionsbyplace.put(place, connection);
        //
        return connection;
    }
    //
    private boolean canUse()
    {
        long now = System.currentTimeMillis();
        long age = now - lastuse;
        //
        return age < SOCKET_TIMEOUT;
    }
    /**
     * Sends a Message on this Connection
     *
     * @param message Message
     * @throws IOException if the message cannot be serialized or sent
     */
    void send(OutgoingMessage message) throws IOException
    {
        var byteout = new ByteArrayOutputStream();
        var limitout = new LimitedOutputStream(byteout, MESSAGE_SIZE);
        var dataout = new DataOutputStream(limitout);
        //
        try
        {
            message.marshal(dataout);
            dataout.flush();
        }
        catch (EOFException e)
        {
            if (message instanceof OutgoingReply)
            {
                var replacementreply = createErrorReply((OutgoingReply) message);
                byteout = new ByteArrayOutputStream();
                dataout = new DataOutputStream(byteout);
                replacementreply.marshal(dataout);
                dataout.flush();
            }
            else
            {
                throw e;
            }
        }
        //
        var body = byteout.toByteArray();
        //
        try
        {
            synchronized (sendlock)
            {
                out.writeInt(body.length);
                out.write(body);
                out.flush();
            }
            //
            lastuse = System.currentTimeMillis();
        }
        catch (IOException e)
        {
            close();
            //
            throw e;
        }
    }
    //
    private static OutgoingReply createErrorReply(OutgoingReply original)
    {
        var call = original.getCall();
        //
        return new OutgoingReply(call, ERROR, EXCESSIVE_MESSAGE_SIZE);
    }
    //
    private void close()
    {
        connectionsbyplace.remove(place, this);
        //
        try
        {
            clientsocket.close();
        }
        catch (IOException ignored)
        {
        }
    }
}