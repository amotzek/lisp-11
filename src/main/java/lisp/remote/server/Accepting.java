package lisp.remote.server;
/*
 * Copyright (C) 2013, 2020 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.IOException;
import java.util.logging.Logger;
/*
 * Created by andreasm 15.10.13 15:51
 */
final class Accepting extends Thread
{
    private static final Logger logger = Logger.getLogger("lisp.remote.server.Server");
    //
    private final Server server;
    //
    Accepting(Server server)
    {
        super();
        //
        this.server = server;
        //
        setDaemon(true);
        setName("accepting");
    }
    //
    @Override
    public void run()
    {
        var protocol = server.getProtocol();
        //
        while (!server.isStopped())
        {
            try
            {
                var connection = protocol.accept();
                server.receiveFrom(connection);
            }
            catch (IOException e)
            {
                logger.warning("cannot accept: " + e.getMessage());
            }
        }
    }
}