package lisp.remote.server;
/*
 * Copyright (C) 2013, 2016, 2020 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.Symbol;
import lisp.Place;
import java.util.HashSet;
import java.util.logging.Logger;
/*
 * Created by andreas-motzek@t-online.de 21.10.13 07:34
 */
final class Expiring extends Thread
{
    private static final Symbol ERROR = Symbol.createSymbol("io-error");
    private static final Chars CALL_EXPIRED = new Chars("call expired");
    //
    private static final Logger logger = Logger.getLogger("lisp.remote.server.Server");
    //
    private final Server server;
    //
    Expiring(Server server)
    {
        super();
        //
        this.server = server;
        //
        setDaemon(true);
        setName("expiring");
    }
    //
    @Override
    public void run()
    {
        try
        {
            while (!server.isStopped())
            {
                sleep();
                expire();
            }
        }
        catch (InterruptedException ignored)
        {
        }
    }
    //
    private void expire()
    {
        var protocol = server.getProtocol();
        var places = getPlaces();
        var calls = protocol.getCalls();
        //
        for (var call : calls)
        {
            var callee = call.getCallee();
            //
            if (places.contains(callee)) continue;
            //
            var id = call.getId();
            call = protocol.removeCall(id);
            //
            if (call == null) continue;
            //
            var fail = call.getFailureContinuation();
            //
            if (fail == null) continue;
            //
            fail.fail(ERROR, CALL_EXPIRED);
            var infobuilder = new StringBuilder();
            infobuilder.append("call #");
            infobuilder.append(id);
            infobuilder.append(" expired");
            logger.warning(infobuilder.toString());
        }
    }
    //
    private HashSet<Place> getPlaces()
    {
        var protocol = server.getProtocol();
        var placecollection = server.getPlaces();
        var placeset = new HashSet<>(placecollection);
        placeset.add(protocol.getHere());
        //
        return placeset;
    }
    //
    private void sleep() throws InterruptedException
    {
        var protocol = server.getProtocol();
        sleep(protocol.getExpiryInterval());
    }
}