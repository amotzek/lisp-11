package lisp.remote.server;
/*
 * Copyright (C) 2020 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.concurrent.ThreadFactory;
/*
 * Created by andreasm 11.06.2020
 */
final class IOThreadFactory implements ThreadFactory
{
    @Override
    public Thread newThread(Runnable runnable)
    {
        var thread = new Thread(runnable);
        thread.setPriority(Thread.MAX_PRIORITY);
        thread.setDaemon(true);
        //
        return thread;
    }
}
