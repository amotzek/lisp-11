package lisp.remote.server;
/*
 * Copyright (C) 2020 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.Place;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.remote.protocol.Protocol;
import java.util.logging.Logger;
/*
 * Created by andreasm 07.06.2020
 */
final class Invoking implements Runnable
{
    private static final Symbol ERROR = Symbol.createSymbol("io-error");
    //
    private static final Logger logger = Logger.getLogger("lisp.remote.server.Server");
    //
    private final Protocol protocol;
    private final Place callee;
    private final Sexpression expression;
    private final Environment environment;
    private final SuccessContinuation succeed;
    private final FailureContinuation fail;
    //
    public Invoking(Protocol protocol, Place callee, Sexpression expression, Environment environment, SuccessContinuation succeed, FailureContinuation fail)
    {
        super();
        //
        this.protocol = protocol;
        this.callee = callee;
        this.expression = expression;
        this.environment = environment;
        this.succeed = succeed;
        this.fail = fail;
    }
    //
    public void run()
    {
        try
        {
            var call = protocol.invoke(callee, expression, environment, succeed, fail);
            var infobuilder = new StringBuilder();
            infobuilder.append("sent call #");
            infobuilder.append(call.getId());
            infobuilder.append(" to ");
            infobuilder.append(callee.getName());
            logger.info(infobuilder.toString());
        }
        catch (Exception e) // occurs in Bitbucket pipeline
        {
            fail.fail(ERROR, new Chars("cannot start remote call: " + e.getMessage()));
        }
    }
}
