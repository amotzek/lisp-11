package lisp.remote.server;
/*
 * Copyright (C) 2013, 2020 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Place;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Logger;
/*
 * Created by andreasm 15.10.13 15:51
 */
final class Noticing extends Thread
{
    private static final Logger logger = Logger.getLogger("lisp.remote.server.Server");
    //
    private final Server server;
    private final ReentrantReadWriteLock readwritelock;
    private final HashMap<Place, Long> timestampsbyplace;
    //
    Noticing(Server server)
    {
        super();
        //
        this.server = server;
        //
        readwritelock = new ReentrantReadWriteLock(true);
        timestampsbyplace = new HashMap<>();
        //
        setDaemon(true);
        setName("noticing");
        setPriority(MAX_PRIORITY);
    }
    //
    @Override
    public void run()
    {
        while (!server.isStopped())
        {
            var place = notice();
            //
            if (place == null) continue;
            //
            register(place);
        }
    }
    //
    private Place notice()
    {
        try
        {
            var protocol = server.getProtocol();
            var place = protocol.notice();
            //
            if (place != null)
            {
                logger.info("noticed " + place.getName());
                //
                return place;
            }
        }
        catch (Exception e)
        {
            logger.warning("cannot notice: " + e.getMessage());
        }
        //
        return null;
    }
    //
    private void register(Place place)
    {
        var writelock = readwritelock.writeLock();
        writelock.lock();
        //
        try
        {
            Long timestamp = System.currentTimeMillis();
            timestampsbyplace.put(place, timestamp);
        }
        finally
        {
            writelock.unlock();
        }
    }
    //
    LinkedList<Place> getPlaces()
    {
        var protocol = server.getProtocol();
        var active = new LinkedList<Place>();
        var stale = new LinkedList<Place>();
        var readlock = readwritelock.readLock();
        readlock.lock();
        //
        try
        {
            long now = System.currentTimeMillis();
            //
            for (var entry : timestampsbyplace.entrySet())
            {
                var place = entry.getKey();
                long then = entry.getValue();
                long elapsed = now - then;
                //
                if (elapsed > protocol.getExpiryInterval())
                {
                    stale.addLast(place);
                }
                else
                {
                    active.addLast(place);
                }
            }
        }
        finally
        {
            readlock.unlock();
        }
        //
        if (!stale.isEmpty())
        {
            var writelock = readwritelock.writeLock();
            writelock.lock();
            //
            try
            {
                for (var place : stale)
                {
                    timestampsbyplace.remove(place);
                }
            }
            finally
            {
                writelock.unlock();
            }
        }
        //
        Collections.sort(active);
        //
        return active;
    }
}