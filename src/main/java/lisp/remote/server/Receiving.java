package lisp.remote.server;
/*
 * Copyright (C) 2013, 2016, 2020 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.Symbol;
import lisp.concurrent.RunnableQueueFactory;
import lisp.remote.protocol.IncomingConnection;
import lisp.remote.protocol.IncomingCall;
import lisp.remote.protocol.IncomingReply;
import lisp.remote.protocol.Message;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * Created by andreasm 16.10.13 10:24
 */
final class Receiving implements Runnable
{
    private static final Symbol ERROR = Symbol.createSymbol("io-error");
    private static final Chars CANNOT_UNMARSHAL = new Chars("cannot unmarshal");
    //
    private static final Logger logger = Logger.getLogger("lisp.remote.server.Server");
    //
    private final Server server;
    private final IncomingConnection connection;
    //
    Receiving(Server server, IncomingConnection connection)
    {
        super();
        //
        this.server = server;
        this.connection = connection;
    }
    //
    @Override
    public void run()
    {
        try
        {
            while (!server.isStopped())
            {
                var protocol = server.getProtocol();
                var combinators = server.getCombinators();
                var message = connection.receive(protocol, combinators);
                int type = message.getType();
                //
                switch (type)
                {
                    case Message.TYPE_CALL:
                        executeCall(message);
                        break;
                    //
                    case Message.TYPE_REPLY:
                        executeReply(message);
                        break;
                }
            }
        }
        catch (SocketTimeoutException e)
        {
            logger.info("socket timeout");
        }
        catch (SocketException e)
        {
            logger.info("cannot receive: " + e.getMessage());
        }
        catch (IOException e)
        {
            logger.log(Level.WARNING, "cannot receive", e);
        }
        finally
        {
            connection.close();
        }
    }
    //
    private void executeCall(Message message)
    {
        var call = (IncomingCall) message;
        var environment = call.getEnvironment();
        var expression = call.getExpression();
        var protocol = server.getProtocol();
        //
        if (environment == null || expression == null)
        {
            try
            {
                protocol.reply(call, ERROR, CANNOT_UNMARSHAL);
            }
            catch (IOException e)
            {
                logger.log(Level.WARNING, "cannot reply", e);
            }
            //
            return;
        }
        //
        var sendresponse = new SendResponse(call, server);
        var runnablequeue = RunnableQueueFactory.getConcurrentRunnableQueue();
        expression.enQueueEvaluator(runnablequeue, environment, sendresponse, sendresponse);
        var caller = call.getCaller();
        var infobuilder = new StringBuilder();
        infobuilder.append("received call #");
        infobuilder.append(call.getId());
        infobuilder.append(" from ");
        infobuilder.append(caller.getName());
        infobuilder.append(" with size ");
        infobuilder.append(call.size());
        logger.info(infobuilder.toString());
    }
    //
    private void executeReply(Message message)
    {
        var reply = (IncomingReply) message;
        var call = reply.getCall();
        var callee = call.getCallee();
        var symbol = reply.getSymbol();
        var expression = reply.getExpression();
        var infobuilder = new StringBuilder();
        infobuilder.append("received reply #");
        infobuilder.append(call.getId());
        infobuilder.append(" from ");
        infobuilder.append(callee.getName());
        logger.info(infobuilder.toString());
        //
        if (symbol == null)
        {
            var success = call.getSuccessContinuation();
            success.succeed(expression);
        }
        else
        {
            var fail = call.getFailureContinuation();
            fail.fail(symbol, expression);
        }
    }
}