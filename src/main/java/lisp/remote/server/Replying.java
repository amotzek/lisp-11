package lisp.remote.server;
/*
 * Copyright (C) 2020 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.Symbol;
import lisp.remote.protocol.IncomingCall;
import lisp.remote.protocol.Protocol;
import java.io.IOException;
import java.net.ConnectException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * Created by andreasm 07.06.2020
 */
final class Replying implements Runnable
{
    private static final int MAX_RETRIES = 30;
    //
    private static final Logger logger = Logger.getLogger("lisp.remote.server.Server");
    //
    private final Protocol protocol;
    private final ScheduledExecutorService executor;
    private final IncomingCall call;
    private final Symbol name;
    private final Sexpression value;
    private int retries;
    //
    Replying(Protocol protocol, ScheduledExecutorService executor, IncomingCall call, Symbol name, Sexpression value)
    {
        super();
        //
        this.protocol = protocol;
        this.executor = executor;
        this.call = call;
        this.name = name;
        this.value = value;
    }
    //
    public void run()
    {
        try
        {
            var reply = protocol.reply(call, name, value);
            var caller = call.getCaller();
            var infobuilder = new StringBuilder();
            infobuilder.append("replied call #");
            infobuilder.append(reply.getId());
            infobuilder.append(" to ");
            infobuilder.append(caller.getName());
            logger.info(infobuilder.toString());
            //
            return;
        }
        catch (ConnectException e)
        {
            logger.warning("cannot reply: " +  e.getMessage());
        }
        catch (IOException e)
        {
            logger.log(Level.WARNING, "cannot reply", e);
        }
        //
        if (shouldRetry()) delayedRetry();
    }
    //
    private boolean shouldRetry()
    {
        return retries < MAX_RETRIES;
    }
    //
    private void delayedRetry()
    {
        long delay = protocol.getExpiryInterval() >> (MAX_RETRIES - retries);
        //
        if (delay < 1L) delay = 1L;
        //
        executor.schedule(this, delay, TimeUnit.MILLISECONDS);
        retries++;
    }
}
