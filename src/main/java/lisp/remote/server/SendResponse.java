package lisp.remote.server;
/*
 * Copyright (C) 2013, 2020 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.remote.protocol.IncomingCall;
/*
 * @author andreasm
 */
final class SendResponse implements SuccessContinuation, FailureContinuation
{
    private final IncomingCall call;
    private final Server server;
    //
    SendResponse(IncomingCall call, Server server)
    {
        super();
        //
        this.call = call;
        this.server = server;
    }
    //
    @Override
    public void succeed(Sexpression value)
    {
        server.reply(call, null, value);
    }
    //
    @Override
    public void fail(Symbol name, Sexpression value)
    {
        server.reply(call, name, value);
    }
    //
    public SuccessContinuation getNext()
    {
        return null;
    }
}