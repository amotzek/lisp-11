package lisp.remote.server;
/*
 * Copyright (C) 2013, 2016, 2020 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Combinator;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.Place;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.remote.protocol.IncomingConnection;
import lisp.remote.protocol.IncomingCall;
import lisp.remote.protocol.Protocol;
import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executors;
/**
 * Bundles the threads for noticing and expiring remote Places,
 * accepting new Connections, executing Calls and announcing the local Place.
 *
 * Created by andreasm 15.10.13 14:09
 */
@SuppressWarnings("WeakerAccess")
public final class Server
{
    private static final IOThreadFactory threadfactory = new IOThreadFactory();
    //
    private final Collection<Combinator> combinators;
    private final Protocol protocol;
    private final Noticing noticing;
    private final Accepting accepting;
    private final Announcing announcing;
    private final Expiring expiring;
    private final ScheduledExecutorService sendingexecutor;
    private final ExecutorService receivingexecutor;
    private volatile boolean stopped;
    /**
     * Creates a non-started Server, should only be used in test cases.
     *
     * @param combinators Additional Combinators for Incoming Calls
     * @param groupname Group Name for announcing
     * @param port Port for announcing
     */
    public Server(Collection<Combinator> combinators, String groupname, int port)
    {
        super();
        //
        this.combinators = combinators;
        //
        protocol = new Protocol(groupname, port);
        noticing = new Noticing(this);
        accepting = new Accepting(this);
        announcing = new Announcing(this);
        expiring = new Expiring(this);
        sendingexecutor = Executors.newScheduledThreadPool(0, threadfactory);
        receivingexecutor = Executors.newCachedThreadPool(threadfactory);
    }
    /**
     * Starts the Server. Should only be used in test cases
     *
     * @param awaitannounce if true wait until announces from places were noticed
     * @throws IOException if the Server cannot be started
     * @see ServerFactory#getServer(boolean)
     */
    public void start(boolean awaitannounce) throws IOException
    {
        protocol.init();
        noticing.start();
        accepting.start();
        announcing.start();
        expiring.start();
        //
        if (awaitannounce)
        {
            try
            {
                Thread.sleep(protocol.getAnnouncingInterval());
            }
            catch (InterruptedException ignore)
            {
            }
        }
    }
    /**
     * Stops the Server
     */
    public void stop()
    {
        stopped = true;
        receivingexecutor.shutdown();
        sendingexecutor.shutdown();
        protocol.destruct();
    }
    /**
     * Checks if the Server is stopped
     *
     * @return true if the Server is stopped
     */
    public boolean isStopped()
    {
        return stopped;
    }
    /**
     * Returns the Protocol used by this Server
     *
     * @return Protocol
     */
    public Protocol getProtocol()
    {
        return protocol;
    }
    /**
     * Returns Combinators that are added to the Environment for Incoming Calls
     *
     * @return Combinators
     */
    public Collection<Combinator> getCombinators()
    {
        return combinators;
    }
    /**
     * Returns a list of noticed remote Places
     *
     * @return List of Places
     */
    public Collection<Place> getPlaces()
    {
        return noticing.getPlaces();
    }
    /**
     * Sends an Outgoing Call
     *
     * @param callee Destination for the Call
     * @param expression S-Expression to invoke
     * @param environment Environment in which the S-Expression is evaluated
     * @param succeed Success Continuation
     * @param fail Failure Continuation
     */
    public void invoke(Place callee, Sexpression expression, Environment environment, SuccessContinuation succeed, FailureContinuation fail)
    {
        var invoking = new Invoking(protocol, callee, expression, environment, succeed, fail);
        sendingexecutor.execute(invoking);
    }
    /**
     * Sends an Outgoing Reply
     *
     * @param call Call of the Reply
     * @param name Name in case of a Throw
     * @param value Result
     */
    void reply(IncomingCall call, Symbol name, Sexpression value)
    {
        var replying = new Replying(protocol, sendingexecutor, call, name, value);
        sendingexecutor.execute(replying);
    }
    /**
     * Receives and processes Messages from the Connection
     *
     * @param connection Connection
     */
    void receiveFrom(IncomingConnection connection)
    {
        var receiving = new Receiving(this, connection);
        receivingexecutor.execute(receiving);
    }
}