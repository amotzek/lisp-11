package lisp.traversal;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.AssociativeContainer;
import lisp.List;
import lisp.Sexpression;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedList;
/*
 * Created by 02.05.13 17:57
 */
public final class Traversal
{
    private final IdentityHashMap<Sexpression, Boolean> visited;
    private final LinkedList<Sexpression> agenda;
    private final Sexpression start;
    //
    public Traversal(Sexpression start)
    {
        super();
        //
        this.start = start;
        //
        visited = new IdentityHashMap<>();
        agenda = new LinkedList<>();
    }
    //
    public <R> R accept(Visitor<R> visitor)
    {
        init();
        //
        while (hasNext())
        {
            var sexpression = takeNext();
            //
            if (sexpression == null) continue;
            //
            if (isMarked(sexpression)) continue;
            //
            mark(sexpression);
            //
            R result = visitor.visit(sexpression);
            //
            if (result != null) return result;
            //
            if (sexpression instanceof AssociativeContainer)
            {
                var container = (AssociativeContainer) sexpression;
                enQueueContainer(container);
            }
            else if (sexpression instanceof List)
            {
                var list = (List) sexpression;
                enQueueList(list);
            }
        }
        //
        return null;
    }
    //
    private void init()
    {
        visited.clear();
        agenda.clear();
        agenda.addFirst(start);
    }
    //
    private boolean hasNext()
    {
        return !agenda.isEmpty();
    }
    //
    private Sexpression takeNext()
    {
        return agenda.removeLast();
    }
    //
    private boolean isMarked(Sexpression sexpression)
    {
        return visited.get(sexpression) != null;
    }
    //
    private void mark(Sexpression sexpression)
    {
        visited.put(sexpression, Boolean.TRUE);
    }
    //
    @SuppressWarnings("unchecked")
    private void enQueueContainer(AssociativeContainer container)
    {
        var keys = (Iterator<Sexpression>) container.keyIterator();
        //
        while (keys.hasNext())
        {
            var key = keys.next();
            var value = container.get(key);
            //
            if (container.hasComplexKeys()) agenda.addFirst(key);
            //
            agenda.addFirst(value);
        }
    }
    //
    private void enQueueList(List list)
    {
        var elements = list.elementIterator();
        //
        while (elements.hasNext())
        {
            agenda.addFirst(elements.next());
        }
    }
}