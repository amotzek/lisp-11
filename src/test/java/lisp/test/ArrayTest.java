/*
 * Created on 26.04.2013
 */
package lisp.test;
//
import lisp.CannotEvalException;
import org.junit.Test;
//
public class ArrayTest extends AbstractTest
{
    public ArrayTest()
    {
        super();
    }
    //
    @Test
    public void testMake() throws CannotEvalException
    {
        expectValue("(type-of (make-array (list 1)))", "array");
        expectValue("(array? (make-array (list 2)))", "t");
        expectValue("(array? 22)", "nil");
    }
    //
    @Test
    public void testSetGet() throws CannotEvalException
    {
        expectValue("(let ((a (make-array (list 3)))) (progn (set-array-element a (list 1) (quote eins)) (set-array-element a (list 2) (quote zwo)) (get-array-element a (list 2))))", "zwo");
        expectValue("(let ((a (make-array (list 4)))) (progn (set-array-element a (list 1) (quote eins)) (set-array-element a (list 2) (quote zwo)) (get-array-element a (list 3))))", "nil");
        expectValue("(let ((a (make-array (list 2)))) (progn (set-array-element a (list 1) (quote eins)) (set-array-element a (list 1) (quote e1ns)) (get-array-element a (list 1))))", "e1ns");
    }
    //
    @Test
    public void testBounds() throws CannotEvalException
    {
        expectValue("(let ((a (make-array (list 2 4)))) (set-array-element a (list 1 3) 13))", "13");
        expectValue("(let ((a (make-array (list 2 4)))) (string? (catch nil (set-array-element a (list 2 1) 21))))", "t");
        expectValue("(let ((a (make-array (list 2 4)))) (string? (catch nil (set-array-element a (list 1 4) 14))))", "t");
        expectValue("(let ((a (make-array (list 2 4)))) (string? (catch nil (set-array-element a (list 2 4) 24))))", "t");
    }
    //
    @Test
    public void testDimensions() throws CannotEvalException
    {
        expectValue("(array-dimensions (make-array (list 2)))", "(2)");
        expectValue("(array-dimensions (make-array (list 2 3)))", "(2 3)");
        expectValue("(array-dimensions (make-array (list 2 3 4)))", "(2 3 4)");
    }
    //
    @Test
    public void testMultiDimensional() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((a (make-array (list 2 2 2)))) ");
        builder.append("(progn (set-array-element a (list 0 0 0) 0) ");
        builder.append("(set-array-element a (list 0 1 0) 10) ");
        builder.append("(set-array-element a (list 1 0 0) 100) ");
        builder.append("(set-array-element a (list 0 0 1) 1) ");
        builder.append("(list (get-array-element a (list 0 0 0)) ");
        builder.append("(get-array-element a (list 0 0 1)) ");
        builder.append("(get-array-element a (list 0 1 0)) ");
        builder.append("(get-array-element a (list 0 1 1)) ");
        builder.append("(get-array-element a (list 1 0 0)) ");
        builder.append("(get-array-element a (list 1 0 1)) ");
        builder.append("(get-array-element a (list 1 1 0)) ");
        builder.append("(get-array-element a (list 1 1 1)))))");
        expectValue(builder.toString(), "(0 1 10 nil 100 nil nil nil)");
    }
    //
    @Test
    public void testFreeze() throws CannotEvalException
    {
        expectValue("(let ((a (make-array (list 1 1)))) (frozen? a))", "nil");
        expectValue("(let ((a (make-array (list 1 1)))) (progn (freeze a) (frozen? a)))", "t");
        expectValue("(type-of (catch (quote error) (let ((a (make-array (list 1 1)))) (progn (freeze a) (set-array-element a (list 0 0) 1)))))", "string");
    }
    //
    @Test
    public void testDuplicate() throws CannotEvalException
    {
        expectValue("(let ((a (make-array (list 1 1)))) (progn (freeze a) (frozen? (duplicate a))))", "nil");
        expectValue("(array-dimensions (duplicate (make-array (list 2 3))))", "(2 3)");
        expectValue("(let ((a (make-array (list 3)))) (progn (set-array-element a (list 1) (quote eins)) (set-array-element a (list 2) (quote zwo)) (get-array-element (duplicate a) (list 2))))", "zwo");
    }
}