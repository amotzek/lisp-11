package lisp.test;
//
import lisp.CannotEvalException;
import org.junit.Test;
/**
 * Created by andreasm on 18.11.2012 13:39
 */
public class FutureTest extends AbstractTest
{
    @Test
    public void testGo() throws CannotEvalException
    {
        expectValue("(type-of (go (+ 2 3)))", "future");
    }
    //
    @Test
    public void testAwaitFuture() throws CannotEvalException
    {
        expectValue("(await-future (go (* 2 3 4)))", "24");
    }
    //
    @Test
    public void testAwaitFuture2() throws CannotEvalException
    {
        expectValue("(let ((future (go (* 2 3 4)))) (list (await-future future) (await-future future)))", "(24 24)");
    }
    //
    @Test
    public void testNoWait() throws CannotEvalException
    {
        expectValue("(type-of (go (receive-from-channels nil 10000)))", "future");
    }
    //
    @Test
    public void testLet() throws CannotEvalException
    {
        expectValue("(await-future (let ((a 4) (b 3)) (go (list (receive-from-channels nil 100) (- a b)))))", "(nil 1)");
    }
}