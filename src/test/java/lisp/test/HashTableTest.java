/*
 * Created on 26.04.2013
 */
package lisp.test;
//
import lisp.CannotEvalException;
import org.junit.Test;
//
public class HashTableTest extends AbstractTest
{
    public HashTableTest()
    {
        super();
    }
    //
    @Test
    public void testMake() throws CannotEvalException
    {
        expectValue("(type-of (make-hash-table))", "hash-table");
        expectValue("(hash-table? (make-hash-table))", "t");
        expectValue("(hash-table? 2)", "nil");
    }
    //
    @Test
    public void testPutGet() throws CannotEvalException
    {
        expectValue("(let ((h (make-hash-table))) (progn (put-hash-table-value h 1 (quote eins)) (put-hash-table-value h (quote zwo) 2) (get-hash-table-value h 1)))", "eins");
        expectValue("(let ((h (make-hash-table))) (progn (put-hash-table-value h 1 (quote eins)) (put-hash-table-value h 1 1) (get-hash-table-value h 1)))", "1");
        expectValue("(let ((h (make-hash-table))) (progn (put-hash-table-value h nil 0) (get-hash-table-value h nil)))", "0");
        expectValue("(let ((h (make-hash-table))) (progn (put-hash-table-value h 0 nil) (get-hash-table-value h 0)))", "nil");
        expectValue("(let ((h (make-hash-table))) (progn (put-hash-table-value h 1 (quote eins)) (put-hash-table-value h (quote zwo) 2) (get-hash-table-value h (quote zwo))))", "2");
        expectValue("(let ((h (make-hash-table))) (progn (put-hash-table-value h 1 (quote eins)) (put-hash-table-value h (quote zwo) 2) (get-hash-table-value h 3)))", "nil");
    }
    //
    @Test
    public void testValues() throws CannotEvalException
    {
        expectValue("(let ((h (make-hash-table))) (progn (put-hash-table-value h 1 (quote eins)) (hash-table-values h)))", "((1 eins))");
        expectValue("(let ((h (make-hash-table))) (progn (put-hash-table-value h (quote eins) 1) (put-hash-table-value h (quote zwo) 2) (assoc (quote zwo) (hash-table-values h))))", "(zwo 2)");
        expectValue("(let ((h (make-hash-table))) (progn (put-hash-table-value h (quote eins) 1) (put-hash-table-value h (quote zwo) 2) (assoc (quote eins) (hash-table-values h))))", "(eins 1)");
    }
    //
    @Test
    public void testClear() throws CannotEvalException
    {
        expectValue("(let ((h (make-hash-table))) (progn (put-hash-table-value h 1 (quote eins)) (clear-hash-table h) (get-hash-table-value h 1)))", "nil");
    }
    //
    @Test
    public void testFreeze() throws CannotEvalException
    {
        expectValue("(let ((h (make-hash-table))) (frozen? h))", "nil");
        expectValue("(let ((h (make-hash-table))) (progn (freeze h) (frozen? h)))", "t");
        expectValue("(type-of (catch (quote error) (let ((h (make-hash-table))) (progn (freeze h) (put-hash-table-value h 1 1)))))", "string");
        expectValue("(type-of (catch (quote error) (let ((h (make-hash-table))) (progn (freeze h) (clear-hash-table h)))))", "string");
    }
    //
    @Test
    public void testDuplicate() throws CannotEvalException
    {
        expectValue("(let ((h (make-hash-table))) (progn (freeze h) (frozen? (duplicate h))))", "nil");
        expectValue("(let ((h (make-hash-table))) (progn (put-hash-table-value h 1 (quote eins)) (put-hash-table-value h (quote zwo) 2) (get-hash-table-value (duplicate h) 1)))", "eins");
    }
}