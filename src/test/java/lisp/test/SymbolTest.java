package lisp.test;
//
import lisp.CannotEvalException;
import org.junit.Test;
/**
 * Created on 23.2.2010
 *
 * @author andreasm
 */
public class SymbolTest extends AbstractTest
{
    public SymbolTest()
    {
        super();
    }
    //
    @Test
    public void testCreate() throws CannotEvalException
    {
        int count = 0;
        //
        for (int i = 0; i < 300; i++)
        {
            StringBuilder builder = new StringBuilder();
            builder.append("(disjoint? (quote (g");
            builder.append(Integer.toString(count++));
            builder.append(" g");
            builder.append(Integer.toString(count++));
            builder.append(" g");
            builder.append(Integer.toString(count++));
            builder.append(" g");
            builder.append(Integer.toString(count++));
            builder.append(" g");
            builder.append(Integer.toString(count));
            int duplicate = count++;
            builder.append(")) (quote (g");
            builder.append(Integer.toString(count++));
            builder.append(" g");
            builder.append(Integer.toString(count++));
            builder.append(" g");
            builder.append(Integer.toString(count++));
            builder.append(" g");
            builder.append(Integer.toString(count++));
            builder.append(" g");
            builder.append(Integer.toString(duplicate));
            builder.append(")))");
            expectValue(builder.toString(), "nil");
        }
    }
}