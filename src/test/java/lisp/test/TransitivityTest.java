package lisp.test;
//
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.Closure;
import lisp.Sexpression;
import lisp.combinator.EnvironmentFactory;
import lisp.environment.Environment;
import lisp.parser.Parser;
import org.junit.Before;
import org.junit.Test;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
//
public class TransitivityTest
{
    private static final Logger logger = Logger.getLogger("lisp.tests.Test");
    //
    private Random random;
    private Environment rootenvironment;
    private Environment environment;
    //
    @Before
    public void setUp() throws Exception
    {
        random = SecureRandom.getInstance("SHA1PRNG", "SUN");
        rootenvironment = EnvironmentFactory.createEnvironment();
    }
    //
    @Test
    public void testTransitivity() throws Exception
    {
        int ntries = 3;
        //
        while (ntries > 0)
        {
            setUpEnvironment();
            int nclasses = ntries + 6;
            //
            try
            {
                setUpClasses(nclasses);
                HashSet<String> results = new HashSet<String>();
                //
                for (int ifunction = 0; ifunction < 2 * nclasses; ifunction++)
                {
                    setUpFunction(nclasses, ifunction);
                    String result = callFunction(ifunction);
                    logger.info(result);
                    results.add(result);
                    //
                    assertEquals(1, results.size());
                }
                //
                ntries--;
            }
            catch (CannotEvalException e)
            {
                logger.info(e.getMessage());
            }
        }
    }
    //
    private void setUpEnvironment()
    {
        environment = rootenvironment.copy();
    }
    //
    private void setUpClasses(int nclasses) throws CannotEvalException
    {
        HashSet<Integer> leafs = new HashSet<Integer>();
        HashSet<Integer> roots = new HashSet<Integer>();
        leafs.add(1);
        execute("(defclass c1 ())");
        //
        for (int iclass = 2; iclass < nclasses; iclass++)
        {
            leafs.add(iclass);
            StringBuilder defclass = new StringBuilder();
            defclass.append("(defclass c");
            defclass.append(iclass);
            defclass.append(" (");
            boolean nosuper = true;
            //
            for (int jclass = 1; jclass < iclass; jclass++)
            {
                if (random.nextInt(Math.max(iclass / 2, 2)) == 0)
                {
                    defclass.append(" c");
                    defclass.append(jclass);
                    leafs.remove(jclass);
                    nosuper = false;
                }
            }
            //
            if (nosuper)
            {
                defclass.append(" c1");
                leafs.remove(1);
            }
            //
            defclass.append("))");
            execute(defclass.toString());
        }
        //
        StringBuilder defclass = new StringBuilder();
        defclass.append("(defclass a (");
        //
        for (int kclass : leafs)
        {
            defclass.append(" c");
            defclass.append(kclass);
        }
        //
        defclass.append("))");
        execute(defclass.toString());
    }
    //
    private void setUpFunction(int nclasses, int ifunction) throws CannotEvalException
    {
        ArrayList<Integer> iclasses = new ArrayList<Integer>(nclasses);
        //
        for (int iclass = 1; iclass < nclasses; iclass++)
        {
            iclasses.add(iclass);
        }
        //
        Collections.shuffle(iclasses, random);
        //
        for (int iclass : iclasses)
        {
            if (iclass == 1)
            {
                StringBuilder defmethod = new StringBuilder();
                defmethod.append("(defmethod m");
                defmethod.append(ifunction);
                defmethod.append(" ((p c1)) t \"1\")");
                execute(defmethod.toString());
            }
            else
            {
                StringBuilder defmethod = new StringBuilder();
                defmethod.append("(defmethod m");
                defmethod.append(ifunction);
                defmethod.append(" ((p c");
                defmethod.append(iclass);
                defmethod.append(")) t (concatenate \"");
                defmethod.append(iclass);
                defmethod.append("\" (call-next-method)))");
                execute(defmethod.toString());
            }
        }
    }
    //
    private String callFunction(int ifunction) throws CannotEvalException
    {
        Sexpression result = execute("(m" + ifunction + " (allocate-instance a))");
        Chars chars = (Chars) result;
        //
        return chars.getString();
    }
    //
    private Sexpression execute(String string) throws CannotEvalException
    {
        logger.info(string);
        Parser parser = new Parser(environment, string, 0);
        Sexpression sexpression = parser.getSexpression();
        Closure closure = new Closure(environment, sexpression);
        //
        return closure.eval();
    }
}